# GMAT Plugin Development

A repo for plugin code developed for GMAT

## Description

This repo includes private plugins developed for GMAT in addition to the ones
provided with GMAT.  At the present time, these plugins include only those
developed to work with attitude of a spacecraft.

## Getting started

To use the plugins in this repo, they should be built along with GMAT and its
other plugins.  The easiest way to accomplish is to clone this repo somewhere,
add a symbolic link in the GMAT plugins directory to the plugins you want from
here, link the included CMakeLists.txt file to the GMAT plugins directory, and
rebuild GMAT.  Then add lines of "PLUGIN =" commands to the GMAT startup file
naming the desired plugins.  

## Installation

Once you have GMAT successfully building, the following commands should bring 
in the available plugins and integrate them into GMAT for immediate use:


```
set GMATCoreDir = place where you have installed the GMAT source
cd to desired directory for developing these plugins
set NewDir = $cwd/gmat-plugin-development
git clone git@gitlab.com:steelheadj/gmat-plugin-development.git
cd $GMATCoreDir/plugins
mv CmakeLists.txt CmakeLists.txt.original
ln -s ${NewDir}/* .
cd $GMATCoreDir
make
cat << EOT >> $GMATCoreDir/bin/gmat_startup_file.txt

# Home-brewed plugins
PLUGIN = ../plugins/libRotater

EOT
```

Note that the actual make command may be cmake3 with some arguments and such.
Make sure you have GMAT building and know what commands are needed prior to
doing the above.  Also note that every time you run the make command, it
overwrites the GMAT startup file and removes the plugin references you added.
Thus, it is helpful to script the build to replace those using the commands
shown above.

