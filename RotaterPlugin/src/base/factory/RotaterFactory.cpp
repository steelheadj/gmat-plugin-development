//$Id$
//------------------------------------------------------------------------------
//                              RotaterFactory
//------------------------------------------------------------------------------
// GMAT: General Mission Analysis Tool.
//
// Author: John R. Wright, SmallSats Team
// Created: Thu Mar 31 13:41:29 PDT 2022
//

#include "RotaterFactory.hpp"

// Include files for all classes this factory may create
#include "ReactionWheel.hpp"


// Constructor
RotaterFactory::RotaterFactory() :
    Factory(Gmat::HARDWARE)
{
    makeCreatableList();
}

// Destructor
RotaterFactory::~RotaterFactory()
{
}

// Copy constructor
RotaterFactory::RotaterFactory(const RotaterFactory& elf) :
    Factory(elf)
{
    makeCreatableList();
}

// Assignment operator
RotaterFactory& RotaterFactory::operator=(const RotaterFactory& elf)
{
    if (this != &elf) {
        Factory::operator=(elf);
        makeCreatableList();
    }

    return *this;
}

// Object creation method
GmatBase* RotaterFactory::CreateObject(const std::string &ofType,
                                const std::string &withName)
{
    return CreateHardware(ofType, withName);
}

// Specialized Hardware object creation method
Hardware* RotaterFactory::CreateHardware(const std::string &ofType,
                                const std::string &withName)
{
    // Create the particular type of object requested
    if (ofType == "ReactionWheel")
        return new ReactionWheel(withName);

    return NULL;   // doesn't match any type of Hardware known by this factory
}

// Helper to set up creatables list
void RotaterFactory::makeCreatableList()
{
    if (creatables.empty()) {
        creatables.push_back("ReactionWheel");
    }

    return;
}
