//$Id$
//------------------------------------------------------------------------------
//                              RotaterFactory
//------------------------------------------------------------------------------
// GMAT: General Mission Analysis Tool.
//
// Author: John R. Wright, SmallSats Team
// Created: Thu Mar 31 13:41:29 PDT 2022
//
/**
 * Definition of the factory used to create commands of the following types:
    ReactionWheel
*/

#ifndef RotaterFactory_hpp
#define RotaterFactory_hpp

#include "RotaterDefs.hpp"
#include "Factory.hpp"


/**
 * Factory class
 */
class ROTATER_API RotaterFactory : public Factory
{
public:
    /// Default factory constructor
    /**
    * Default constructor for the RotaterFactory class
    */
    RotaterFactory();

    /// Destructor
    /**
    * Destructor for the RotaterFactory class
    */
    virtual ~RotaterFactory();

    ///Copy constructor
    /**
    * Copy constructor for the RotaterFactory class
    *
    * @param elf The factory copied here
    */
    RotaterFactory(const RotaterFactory& elf);

    /// Assignment operator
    /**
    * Assignment operator for the RotaterFactory class
    *
    * @param elf The factory copied to this one
    * @return this instance, set to match elf
    */
    RotaterFactory& operator=(const RotaterFactory& elf);

    /// Generic object creator
    /**
    * Creation method for GMAT types created by this factory
    * @param ofType The subtype of the object to be created
    * @param withName The name of type to be created
    *
    * @return A newly created object (or NULL if this factory doesn't create
    *         the requested type)
    */
    virtual GmatBase*        CreateObject(const std::string &ofType,
                                        const std::string &withName = "");

    /// Specific object creator
    /**
    * Creation method for GMAT types created by this factory
    * @param ofType The subtype of the object to be created
    * @param withName The name of type to be created
    *
    * @return A newly created Hardware (or NULL if this factory doesn't create
    *         the requested type)
    */
    virtual Hardware*       CreateHardware(const std::string &ofType,
                                        const std::string &withName = "");

protected:

    /// Common method to create creatable list
    /**
    * The makeCreatableList method builds the specific list of creatable types
    * that this factory handles.  By implementing this method, all the constructors
    * can call it in one place rather than having identical code scattered willy-
    * nilly.
    */
    virtual void makeCreatableList();

};

#endif /* RotaterFactory_hpp */
