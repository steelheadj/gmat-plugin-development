//$Id$
//------------------------------------------------------------------------------
//                              PorpoiseCommandFactory
//------------------------------------------------------------------------------
// GMAT: General Mission Analysis Tool.
//
// Author: John R. Wright, SmallSats Team
// Created: Thu Mar 31 13:35:46 PDT 2022
//
/**
 * Definition of the factory used to create commands of the following types:
    BeginPorpoising
    EndPorpoising
*/

#ifndef PorpoiseCommandFactory_hpp
#define PorpoiseCommandFactory_hpp

#include "PorpoiseCommandDefs.hpp"
#include "Factory.hpp"


/**
 * Factory class
 */
class PORPOISECOMMAND_API PorpoiseCommandFactory : public Factory
{
public:
    /// Default factory constructor
    /**
    * Default constructor for the PorpoiseCommandFactory class
    */
    PorpoiseCommandFactory();

    /// Destructor
    /**
    * Destructor for the PorpoiseCommandFactory class
    */
    virtual ~PorpoiseCommandFactory();

    ///Copy constructor
    /**
    * Copy constructor for the PorpoiseCommandFactory class
    *
    * @param elf The factory copied here
    */
    PorpoiseCommandFactory(const PorpoiseCommandFactory& elf);

    /// Assignment operator
    /**
    * Assignment operator for the PorpoiseCommandFactory class
    *
    * @param elf The factory copied to this one
    * @return this instance, set to match elf
    */
    PorpoiseCommandFactory& operator=(const PorpoiseCommandFactory& elf);

    /// Generic object creator
    /**
    * Creation method for GMAT types created by this factory
    * @param ofType The subtype of the object to be created
    * @param withName The name of type to be created
    *
    * @return A newly created object (or NULL if this factory doesn't create
    *         the requested type)
    */
    virtual GmatBase*        CreateObject(const std::string &ofType,
                                        const std::string &withName = "");

    /// Specific object creator
    /**
    * Creation method for GMAT types created by this factory
    * @param ofType The subtype of the object to be created
    * @param withName The name of type to be created
    *
    * @return A newly created GmatCommand (or NULL if this factory doesn't create
    *         the requested type)
    */
    virtual GmatCommand*       CreateCommand(const std::string &ofType,
                                        const std::string &withName = "");

protected:

    /// Common method to create creatable list
    /**
    * The makeCreatableList method builds the specific list of creatable types
    * that this factory handles.  By implementing this method, all the constructors
    * can call it in one place rather than having identical code scattered willy-
    * nilly.
    */
    virtual void makeCreatableList();

};

#endif /* PorpoiseCommandFactory_hpp */
