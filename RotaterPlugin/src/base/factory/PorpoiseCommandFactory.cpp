//$Id$
//------------------------------------------------------------------------------
//                              PorpoiseCommandFactory
//------------------------------------------------------------------------------
// GMAT: General Mission Analysis Tool.
//
// Author: John R. Wright, SmallSats Team
// Created: Thu Mar 31 13:35:46 PDT 2022
//

#include "PorpoiseCommandFactory.hpp"

// Include files for all classes this factory may create
#include "BeginPorpoising.hpp"
#include "EndPorpoising.hpp"


// Constructor
PorpoiseCommandFactory::PorpoiseCommandFactory() :
    Factory(Gmat::COMMAND)
{
    makeCreatableList();
}

// Destructor
PorpoiseCommandFactory::~PorpoiseCommandFactory()
{
}

// Copy constructor
PorpoiseCommandFactory::PorpoiseCommandFactory(const PorpoiseCommandFactory& elf) :
    Factory(elf)
{
    makeCreatableList();
}

// Assignment operator
PorpoiseCommandFactory& PorpoiseCommandFactory::operator=(const PorpoiseCommandFactory& elf)
{
    if (this != &elf) {
        Factory::operator=(elf);
        makeCreatableList();
    }

    return *this;
}

// Object creation method
GmatBase* PorpoiseCommandFactory::CreateObject(const std::string &ofType,
                                const std::string &withName)
{
    return CreateCommand(ofType, withName);
}

// Specialized Command object creation method
GmatCommand* PorpoiseCommandFactory::CreateCommand(const std::string &ofType,
                                const std::string &withName)
{
    // Create the particular type of object requested
    if (ofType == "BeginPorpoising")
        return new BeginPorpoising(withName);
    if (ofType == "HaltPorpoising")
        return new EndPorpoising(withName);

    return NULL;   // doesn't match any type of Command known by this factory
}

// Helper to set up creatables list
void PorpoiseCommandFactory::makeCreatableList()
{
    if (creatables.empty()) {
        creatables.push_back("BeginPorpoising");
        creatables.push_back("HaltPorpoising");
    }

    return;
}
