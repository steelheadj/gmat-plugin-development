#include "Attitude.hpp"
#include "AttitudeException.hpp"
#include "MessageInterface.hpp"
#include "StringUtil.hpp"

#include "Spacecraft.hpp"

#include "Porpoising.hpp"

// Constants
//#define DEBUG_NADIR_POINTING_COMPUTE

/// Labels used for the Porpoising element parameters.
const std::string
Porpoising::PARAMETER_TEXT[PorpoisingParamCount - NadirPointingParamCount] =
{
    "MaxPorpoisingSlew",
    "PorpoisingSlewRate",
    "MaxPorpoisingSlewRate",
    "PorpoisingSlewAxis",
};

/// Types of the parameters used by Porpoising
const Gmat::ParameterType
Porpoising::PARAMETER_TYPE[PorpoisingParamCount - NadirPointingParamCount] =
{
    Gmat::REAL_TYPE,        // "MaxPorpoisingSlew"
    Gmat::REAL_TYPE,        // "PorpoisingSlewRate"
    Gmat::REAL_TYPE,        // "MaxPorpoisingSlewRate"
    Gmat::RVECTOR_TYPE,     // "PorpoisingSlewAxis"
};

//------------------------------------------------------------------------------
// Constructor
Porpoising::Porpoising(const std::string &itsName ) :
    NadirPointing(itsName),
    maxPorpoisingSlew(0.0),
    porpoisingSlewRate(0.0),
    maxPorpoisingSlewRate(0.0),
    isSlewing(false),
    currentSlew(0.0),
    lastNodeTime(0.0),
    started(false)
{

    objectTypeNames.push_back("Porpoising");
    attitudeModelName         = "Porpoising";

    for (Integer i = NadirPointingParamCount; i < PorpoisingParamCount; ++i)
        parameterWriteOrder.push_back(i);

    // Initialize axis of rotation
    porpoisingSlewAxis.Set(0.0, 0.0, 0.0);
}

// copy constructor
Porpoising::Porpoising(const Porpoising& att) :
	NadirPointing(att),
    maxPorpoisingSlew(att.maxPorpoisingSlew),
    porpoisingSlewRate(att.porpoisingSlewRate),
    maxPorpoisingSlewRate(att.maxPorpoisingSlewRate),
    isSlewing(att.isSlewing),
    porpoisingSlewAxis(att.porpoisingSlewAxis),
    currentSlew(att.currentSlew),
    lastNodeTime(att.lastNodeTime),
    started(att.started),
    lastSlewRate(att.lastSlewRate),
    currentSlewRate(att.currentSlewRate),
    lastX(att.lastX),
    angleOffset(att.angleOffset)
{ 
fprintf(stdout,"Copied Porpoising\n");
    for (Integer i = NadirPointingParamCount; i < PorpoisingParamCount; ++i)
        parameterWriteOrder.push_back(i);
}

//------------------------------------------------------------------------------
//  GmatBase* Clone() const
//------------------------------------------------------------------------------
/**
 * This method returns a clone of the Porpoising.
 *
 * @return clone of the Porpoising.
 *
 */
//------------------------------------------------------------------------------
GmatBase* Porpoising::Clone() const
{
	return (new Porpoising(*this));
}

// Porpoising Destructor
GMAT_API Porpoising::~Porpoising() {
}

// Compute attitude using Real Time
void Porpoising::ComputeCosineMatrixAndAngularVelocity(Real atTime) {
    Real tX;

    // NadirPointing::ComputeCosineMatrixAndAngularVelocity(atTime);
#ifdef DEBUG_NADIR_POINTING_COMPUTE
    MessageInterface::ShowMessage("In Porpoising, computing at time %le\n",
            atTime);
    MessageInterface::ShowMessage("attitudeConstraintType = %s\n", attitudeConstraintType.c_str());
    if (!owningSC) MessageInterface::ShowMessage("--- owningSC is NULL!!!!\n");
    if (!refBody) MessageInterface::ShowMessage("--- refBody is NULL!!!!\n");
#endif

    if (!isInitialized || needsReinit)
    {
        Initialize();
        if (!isInitialized) {
            std::string attEx = "Porpoising attitude unable to initialize";
            throw AttitudeException(attEx);
        }
    }

    // Need to do check for unset reference body here since the needsReinit
    // flag may not have been set
    if (!refBody)
    {
        std::string attEx  = "Reference body ";
        attEx             += refBodyName + " not defined for attitude of type \"";
        attEx             += "Porpoising\" on spacecraft \"";
        attEx             += owningSC->GetName() + "\".";
        throw AttitudeException(attEx);
    }

    /// using a spacecraft object, *owningSC
    A1Mjd theTime(atTime);

    Rvector6 scState = ((SpaceObject*) owningSC)->GetMJ2000State(theTime);
    Rvector6 refState = refBody->GetMJ2000State(theTime);

    Rvector3 pos(scState[0] - refState[0], scState[1] - refState[1],  scState[2] - refState[2]);
    Rvector3 vel(scState[3] - refState[3], scState[4] - refState[4],  scState[5] - refState[5]);

#ifdef DEBUG_NADIR_POINTING_COMPUTE
    MessageInterface::ShowMessage("   scState  = %s\n", scState.ToString().c_str());
    MessageInterface::ShowMessage("   refState = %s\n", refState.ToString().c_str());
    MessageInterface::ShowMessage("   pos      = %s\n", pos.ToString().c_str());
    MessageInterface::ShowMessage("   vel      = %s\n", vel.ToString().c_str());
#endif

    // Error message
    std::string attErrorMsg = "Nadir Pointing attitude model is singular and/or ";
    attErrorMsg            += "undefined for Spacecraft \"";
    attErrorMsg            += owningSC->GetName() + "\".";
    if ((bodyAlignmentVector.GetMagnitude() < 1.0e-5 )  ||
            (bodyConstraintVector.GetMagnitude() < 1.0e-5)  ||
            (bodyAlignmentVector.GetUnitVector()*
             bodyConstraintVector.GetUnitVector()
             > ( 1.0 - 1.0e-5))                        ||
            (pos.GetMagnitude() < 1.0e-5)                   ||
            (vel.GetMagnitude() < 1.0e-5)                   ||
            (Cross(pos,vel).GetMagnitude() < 1.0e-5)        )
    {
        throw AttitudeException(attErrorMsg);
    }

    Rvector3 normal = Cross(pos,vel);

    Rvector3 xhat = pos/pos.GetMagnitude();
    Rvector3 yhat = normal/normal.GetMagnitude();
    Rvector3 zhat = Cross(xhat,yhat);

    Rvector3 referenceVector;
    Rvector3 constraintVector;


#ifdef DEBUG_NADIR_POINTING_COMPUTE
    MessageInterface::ShowMessage("   refBody = <%s>\n", refBody->GetName().c_str());
    MessageInterface::ShowMessage("   normal  = %s\n", normal.ToString().c_str());
    MessageInterface::ShowMessage("   xhat    = %s\n", xhat.ToString().c_str());
    MessageInterface::ShowMessage("   yhat    = %s\n", yhat.ToString().c_str());
    MessageInterface::ShowMessage("   zhat    = %s\n", zhat.ToString().c_str());
#endif

    // RiI calculation (from inertial to LVLH)
    Rmatrix33 RIi;
    RIi(0,0) = xhat(0);
    RIi(1,0) = xhat(1);
    RIi(2,0) = xhat(2);
    RIi(0,1) = yhat(0);
    RIi(1,1) = yhat(1);
    RIi(2,1) = yhat(2);
    RIi(0,2) = zhat(0);
    RIi(1,2) = zhat(1);
    RIi(2,2) = zhat(2);

    if(isSlewing) {

        if(isStopping) {
            currentSlewRate = lastSlewRate;
            if(currentSlew > 0.0) {
                if(currentSlewRate > 0.0) {
                    currentSlewRate = -currentSlewRate;
                }
            } else if(currentSlew < 0.0) {
                if(currentSlewRate < 0.0) {
                    currentSlewRate = -currentSlewRate;
                }
            }
            if(lastSlew * currentSlew <= 0.0) {
                currentSlew = 0.0;
                isStopping = false;
                isSlewing = false;
            } else {
                currentSlew += (atTime - lastTime) * currentSlewRate;
            }
        } else {
            double localSlewLimit;
            double semiMajorAxis = ((SpaceObject*) owningSC)->GetRealParameter("SMA");
            double orbitTime = 2.0 * M_PI * sqrt(semiMajorAxis*semiMajorAxis*semiMajorAxis /
                    3.986004418e5); // grav constant of earth in km^3 s^-2
//fprintf(stdout,"SMA:%f  gives orbitTime:%fsec or %fhrs\n", semiMajorAxis, orbitTime, orbitTime/3600);
            localSlewLimit = porpoisingSlewRate / 4 * orbitTime;
            if(localSlewLimit > maxPorpoisingSlew) localSlewLimit = maxPorpoisingSlew;
            double nodeOffset = ((SpaceObject*) owningSC)->GetRealParameter("TA") +
                                ((SpaceObject*) owningSC)->GetRealParameter("AOP");
            currentSlew = -cos(nodeOffset / 180.0 * M_PI) * localSlewLimit;
        }
    }
//fprintf(stdout,"At time is %.4f slew set to %.2f degrees whilst\n", atTime, currentSlew);

    // Set alignment/constraint vector in body frame
    if ( attitudeConstraintType == "OrbitNormal" )
    {
        referenceVector[0] = -1;
        referenceVector[1] = 0;
        referenceVector[2] = 0;

        constraintVector[0] = 0;
        constraintVector[1] = cos(currentSlew / 180.0 * M_PI);
        constraintVector[2] = sin(currentSlew / 180.0 * M_PI);
    }
    else if ( attitudeConstraintType == "Velocity" )
    {
        referenceVector[0] = -1;
        referenceVector[1] = 0;
        referenceVector[2] = 0;

        constraintVector[0] = 0;
        constraintVector[1] = 0;
        constraintVector[2] = -1;
    }

    // RBi calculation using TRIAD (from LVLH to body frame)
    //Rmatrix33 RiB = TRIAD( bodyAlignmentVector, bodyConstraintVector, referenceVector, constraintVector );
    Rmatrix33 RiB = TRIAD( bodyAlignmentVector, bodyConstraintVector, porpoisingSlewAxis, constraintVector );

    // the rotation matrix (from body to inertial)
    dcm = RIi*RiB;
    // the final rotation matrix (from inertial to body)
    dcm =  dcm.Transpose();

    // We do NOT calculate angular velocity for Nadir.
    // TODO : Throw AttitudeException?? write warning - but what to return?
    /*
       Rmatrix33 wxIBB = - RBi*RiIDot*dcm.Transpose();
       Rvector3 wIBB;
       wIBB.Set(wxIBB(2,1),wxIBB(0,2),wxIBB(1,0));
       */
}

// Initializes the Porpoising maneuver and validates settings
bool GMAT_API Porpoising::Initialize(void) {
	bool  retValue = NadirPointing::Initialize();

    // Make sure axis is specified reasonably and normalize it
    Real length = 0.0;
    for(int i=0; i<porpoisingSlewAxis.GetSize(); i++) {
        length += porpoisingSlewAxis[i] * porpoisingSlewAxis[i];
    }
    if(length < 1.0e-10) {
        isInitialized = false;
        return(isInitialized);
    }
    length = sqrt(length);
    for(int i=0; i<porpoisingSlewAxis.GetSize(); i++) {
        porpoisingSlewAxis[i] /= length;
    }

    // Make sure specified rate is supported by rotaters

	return(retValue);
}

//------------------------------------------------------------------------------
//  Integer  GetParameterID(const std::string &str) const
//------------------------------------------------------------------------------
/**
 * This method returns the parameter ID, given the input parameter string.
 *
 * @param <str> string for the requested parameter.
 *
 * @return ID for the requested parameter.
 *
 */
//------------------------------------------------------------------------------
Integer Porpoising::GetParameterID(const std::string &str) const
{
   for (Integer i = getBaseParameterCount(); i < getParameterCount(); i++)
   {
       if (str == PARAMETER_TEXT[i - getBaseParameterCount()]) {
           fprintf(stdout, "Parameter %s is id:%d\n", str.c_str(), i);
           return i;
       }
   }

   return NadirPointing::GetParameterID(str);
}

// And their names and types
Gmat::ParameterType Porpoising::GetParameterType(const Integer id) const {
    if (id >= getBaseParameterCount() && id < getParameterCount())
        return PARAMETER_TYPE[id - getBaseParameterCount()];

    return(NadirPointing::GetParameterType(id));
}

// Sets the specified real parameter to the specified value
Real Porpoising::SetRealParameter(const Integer id, const Real value) {
    switch (id) {
        case MAXPORPOISINGSLEW:
            if(value >= 0.0) {
                maxPorpoisingSlew = value;
            } else {
                AttitudeException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "MaxPorpoisingSlew", "0.0 <= Real Number");
                throw he;
            }
            return(maxPorpoisingSlew);
        case PORPOISINGSLEWRATE:
            if(value >= 0.0) {
                porpoisingSlewRate = value;
            } else {
                AttitudeException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "PorpoisingSlewRate", "0.0 <= Real Number");
                throw he;
            }
            return(porpoisingSlewRate);
    }

    return NadirPointing::SetRealParameter(id, value);
}



// Sets the specified bool parameter to the specified value
bool Porpoising::SetBooleanParameter(const Integer id, const bool value) {
	return(NadirPointing::SetBooleanParameter(id, value));
}

   // Sets the specified Boolean parameter to the specified value
bool GMAT_API Porpoising::SetBooleanParameter(const std::string &label, const bool value) {
	Integer id = GetParameterID(label);
    return SetBooleanParameter(id, value);
}

   // Sets the specified real parameter to the specified value
Real GMAT_API Porpoising::SetRealParameter(const Integer id, const Real value, const Integer index) {
    if(id == PORPOISINGSLEWAXIS) {
        if(index < 0 || index >= porpoisingSlewAxis.GetSize()) {
            throw AttitudeException("Index out of bounds setting the porpoising axis on " +
                instanceName);
        }

        porpoisingSlewAxis[index] = value;
        return(porpoisingSlewAxis[index]);
    }

    return(NadirPointing::SetRealParameter(id, value, index));
}

       // And their names and types
std::string GMAT_API Porpoising::GetParameterText(const Integer id) const {
    if (id >= getBaseParameterCount() && id < getParameterCount())
        return PARAMETER_TEXT[id - getBaseParameterCount()];

    return(NadirPointing::GetParameterText(id));
}

// Kicks off the porpoising maneuver at the current orientation
bool GMAT_API Porpoising::StartPorpoising(void) {
    bool retValue = true;

    Spacecraft *sc = (Spacecraft*) owningSC;
    if(!sc) {
        throw AttitudeException("No Spacecraft set for Porpoising Attitude:" +
                instanceName);
    }
    double atTime = sc->GetEpoch();
    fprintf(stdout,"Start time:%s\n", sc->GetEpochString().c_str());

    // Start the maneuver
    isSlewing = true;
    isStopping = false;

    // Record the current time and slew
    lastTime = atTime;
    lastSlew = currentSlew;

    return(retValue);
}

// Terminates the current porpoising maneuver at the current orientation
bool GMAT_API Porpoising::StopPorpoising(void) {
    bool  retValue = true;

    // Stop the maneuver
    isSlewing = false;
    isStopping = false;
    started = false;

    return(retValue);
}

// Terminates the current porpoising maneuver at the zero orientation
bool GMAT_API Porpoising::SlowPorpoising(void) {
    Spacecraft *sc = (Spacecraft*) owningSC;
    fprintf(stdout,"Stop time:%s\n", sc->GetEpochString().c_str());
    
    if(isSlewing) {
        // Note that we need to go to 0
        isStopping = true;
        started = false;
        return(true);
    } else{
        // If we are not currently porpoising, it is wrong to go back to 0
        return(false);
    }
}

