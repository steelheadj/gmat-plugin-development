
/**
*********************************************************
  Class Porpoising
  Created by john
  Created Thu Mar 17 04:20:12 PDT 2022
*********************************************************
  Modification History
  Date        Programmer       Change

*********************************************************
*********************************************************
*/

#ifndef _Porpoising_H_
#define _Porpoising_H_

// includes block
#include "NadirPointing.hpp"
#include "PorpoisingDefs.hpp"

// declarations block

/// Implements an attitude module to do porpoising
/**
    The Porpoising class implements an attitude module to do porpoising.
It is based on the NadirPointing class in that it points one axis to the
center of its reference body and rotates around that axis.
*/

class GMAT_API Porpoising : public NadirPointing
{     

    private:

    protected:

        virtual void ComputeCosineMatrixAndAngularVelocity(Real atTime);

        /// Published parameters of Porpoisings
        enum {
            MAXPORPOISINGSLEW = NadirPointingParamCount,
            PORPOISINGSLEWRATE,
            MAXPORPOISINGSLEWRATE,
            PORPOISINGSLEWAXIS,
            PorpoisingParamCount
        };

        /// And their names and types
        static const std::string
            PARAMETER_TEXT[PorpoisingParamCount - NadirPointingParamCount];
        static const Gmat::ParameterType
            PARAMETER_TYPE[PorpoisingParamCount - NadirPointingParamCount];

        // Parameter access methods - overridden from Attitude
        virtual Integer GetParameterID(const std::string &str) const;
        virtual Gmat::ParameterType GetParameterType(const Integer id) const;
        virtual std::string GetParameterText(const Integer id) const;

        /// Virtual method to return number of parameters to check
        virtual Integer getParameterCount() const {
            return(PorpoisingParamCount);
        }

        /// Virtual method to get base class parameter count
        virtual Integer getBaseParameterCount() const {
            return(NadirPointingParamCount);
        }

        /// Maximum slew angle in degrees
        Real    maxPorpoisingSlew;

        /// Desired maximum slew rate in degrees per second
        Real    porpoisingSlewRate;

        /// Maximum slew rate allowed in degrees per second
        Real    maxPorpoisingSlewRate;

        /// Axis around which to porpoise
        Rvector3 porpoisingSlewAxis;

        /// State
        bool    isSlewing;
        bool    isStopping;

        /// Has propagator called us even once
        bool    started;

        /// Current angle at which spacecraft is slewed
        Real    currentSlew;
        Real    lastSlew;

        /// Time at which last node was crossed
        Real    lastNodeTime;
        /// Time of last known attitude
        Real    lastTime;

        /// Last actual slew rate
        Real    lastSlewRate;
        /// Current slew rate
        Real    currentSlewRate;

        /// Angle offset of initial apsis from reference body coordinate frame
        Real    angleOffset;
        /// Cheater value
        Real    lastX;

    public:

	/// Kicks off the porpoising maneuver at the current orientation
	/**
	The StartPorpoising method kicks off the porpoising maneuver
	@return Returns true if successful and false otherwise
	*/
	bool StartPorpoising(void);

	/// Terminates the current porpoising maneuver at the current orientation
	/**
	The StopPorpoising method terminates the current porpoising maneuver at 
    the current orientation.
	@return Returns true if successful and false otherwise
	*/
	bool StopPorpoising(void);

	/// Terminates the current porpoising maneuver at the zero orientation
	/**
	The SlowPorpoising method terminates the current porpoising maneuver at 
    the current orientation and rotates back to zero slew and ends.
	@return Returns true if successful and false otherwise
	*/
	bool SlowPorpoising(void);

    /// # Insert single line doxygen comment here
    /**
    The Rotater::Initialize method # Insert multiline doxygen comment here
    @return Returns # value description
    */
    bool Initialize(void);

    /// Sets the specified real parameter to the specified value
    /**
    The SetRealParameter method # Insert multiline doxygen comment here
    @return Returns # value description
    @param id const Integer # Argument description
    @param value const Real # Argument description
    @param index const Integer # Argument description
    */
    virtual Real SetRealParameter(const Integer id, const Real value);
    virtual Real SetRealParameter(const Integer id, const Real value, const Integer index);

    /// Sets the specified Boolean parameter to the specified value
    /**
    The SetBooleanParameter method sets the specified Boolean parameter to the specified value
    @return Returns the boolean value that was set
    @param id const Integer Index of value in parameter array
    @param label const string Name of value in parameter array
    @param value const Boolean Value to set parameter to
    */
    virtual bool SetBooleanParameter(const Integer id, const bool value);
    virtual bool SetBooleanParameter(const std::string &label, const bool value);

    /// Clones this instance of a Porpoising
    /**
    The Clone method  Insert multiline doxygen comment herones this instance of a Porpoising
    @return Returns newly cloned copy of this object
    */
    virtual GmatBase* Clone(void) const;


	/// Porpoising Constructor
	/**
	Constructor for the Porpoising class
	*/
    Porpoising(const std::string &itsName = "");

    /// Porpoising copy constructor
    Porpoising(const Porpoising& att);

	/// Porpoising Destructor
	/**
	Destructor for the Porpoising class
	*/
	virtual	~Porpoising();

	/// Porpoising Self test method
	/**
	Self test method for the Porpoising class
	Silently returns TRUE if successful and returns FALSE and 
	outputs descriptive text if unsuccessful.
	*/
	static int self_test(void);

};

#endif

