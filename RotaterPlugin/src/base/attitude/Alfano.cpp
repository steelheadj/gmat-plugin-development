#include "Attitude.hpp"
#include "AttitudeException.hpp"
#include "MessageInterface.hpp"
#include "StringUtil.hpp"

#include "Spacecraft.hpp"

#include "Alfano.hpp"

// Constants
//#define DEBUG_NADIR_POINTING_COMPUTE

Real Alfano::uvalues[] = {0.1, 0.1008, 0.1016, 0.1024, 0.1031, 0.1039, 0.1047, 0.1055, 0.1063, 0.107, 0.1078, 0.1086, 0.1094, 0.1102, 0.1109, 0.1117, 0.1126, 0.1134, 0.1141, 0.1149, 0.1156, 0.1165, 0.1172, 0.118, 0.1188, 0.1197, 0.1204, 0.1212, 0.122, 0.1228, 0.1236, 0.1244, 0.1252, 0.126, 0.1268, 0.1276, 0.1284, 0.1292, 0.13, 0.1308, 0.1316, 0.1324, 0.1333, 0.1341, 0.1348, 0.1357, 0.1364, 0.1373, 0.1381, 0.1389, 0.1397, 0.1405, 0.1414, 0.1422, 0.1431, 0.1439, 0.1447, 0.1456, 0.1463, 0.1472, 0.148, 0.1489, 0.1497, 0.1505, 0.1514, 0.1522, 0.153, 0.1539, 0.1548, 0.1556, 0.1565, 0.1573, 0.1582, 0.159, 0.1598, 0.1607, 0.1616, 0.1625, 0.1634, 0.1643, 0.1651, 0.166, 0.1669, 0.1677, 0.1686, 0.1695, 0.1704, 0.1713, 0.1722, 0.1731, 0.1739, 0.1749, 0.1757, 0.1766, 0.1775, 0.1785, 0.1793, 0.1803, 0.1812, 0.1821, 0.183, 0.1839, 0.1849, 0.1858, 0.1867, 0.1876, 0.1886, 0.1895, 0.1905, 0.1914, 0.1924, 0.1933, 0.1943, 0.1952, 0.1962, 0.1971, 0.198, 0.1991, 0.2, 0.201, 0.202, 0.2029, 0.2039, 0.2049, 0.2059, 0.2069, 0.2078, 0.2089, 0.2099, 0.2108, 0.2119, 0.2129, 0.2139, 0.2149, 0.216, 0.217, 0.218, 0.219, 0.22, 0.2211, 0.2222, 0.2232, 0.2243, 0.2253, 0.2263, 0.2274, 0.2285, 0.2295, 0.2306, 0.2317, 0.2328, 0.2338, 0.2349, 0.2361, 0.2372, 0.2382, 0.2393, 0.2405, 0.2416, 0.2428, 0.2438, 0.245, 0.246, 0.2472, 0.2484, 0.2494, 0.2506, 0.2518, 0.253, 0.254, 0.2552, 0.2564, 0.2575, 0.2588, 0.2599, 0.2611, 0.2623, 0.2636, 0.2648, 0.266, 0.2672, 0.2684, 0.2697, 0.2709, 0.2721, 0.2733, 0.2746, 0.2758, 0.277, 0.2784, 0.2796, 0.2809, 0.2821, 0.2835, 0.2846, 0.286, 0.2872, 0.2886, 0.2899, 0.2911, 0.2925, 0.2939, 0.2952, 0.2964, 0.2978, 0.2993, 0.3005, 0.3019, 0.3031, 0.3046, 0.306, 0.3074, 0.3088, 0.3101, 0.3115, 0.3129, 0.3143, 0.3157, 0.3172, 0.3184, 0.3201, 0.3215, 0.3229, 0.3242, 0.3258, 0.3272, 0.3288, 0.3302, 0.3316, 0.3331, 0.3346, 0.3359, 0.3374, 0.339, 0.3404, 0.3421, 0.3435, 0.3451, 0.3466, 0.3482, 0.3496, 0.3511, 0.3527, 0.3545, 0.356, 0.3576, 0.3591, 0.3606, 0.3622, 0.3637, 0.3655, 0.3671, 0.3686, 0.3701, 0.372, 0.3735, 0.3751, 0.3768, 0.3784, 0.3802, 0.3817, 0.3833, 0.3849, 0.3866, 0.3882, 0.39, 0.3918, 0.3934, 0.3949, 0.3967, 0.3983, 0.4001, 0.4016, 0.4035, 0.4053, 0.4071, 0.4087, 0.4105, 0.412, 0.4139, 0.4157, 0.4172, 0.4191, 0.4206, 0.4224, 0.4243, 0.4261, 0.4276, 0.4295, 0.4313, 0.4328, 0.4347, 0.4365, 0.4383, 0.4402, 0.442, 0.4438, 0.4454, 0.4472, 0.4487, 0.4506, 0.4524, 0.4542, 0.4561, 0.4579, 0.4594, 0.4613, 0.4631, 0.4649, 0.4668, 0.4686, 0.4702, 0.472, 0.4738, 0.4757, 0.4775, 0.4793, 0.4809, 0.4827, 0.4845, 0.4864, 0.4879, 0.4897, 0.4916, 0.4934, 0.4952, 0.4968, 0.4986, 0.5004, 0.502, 0.5038, 0.5056, 0.5075, 0.509, 0.5108, 0.5127, 0.5142, 0.516, 0.5179, 0.5197, 0.5212, 0.5228, 0.5246, 0.5264, 0.528, 0.5298, 0.5313, 0.5332, 0.5347, 0.5365, 0.5381, 0.5399, 0.5417, 0.543, 0.5448, 0.5463, 0.5482, 0.5497, 0.5515, 0.5531, 0.5546, 0.5564, 0.5579, 0.5595, 0.561, 0.5628, 0.5644, 0.5659, 0.5674, 0.569, 0.5705, 0.5723, 0.5736, 0.5754, 0.5769, 0.5785, 0.58, 0.5815, 0.583, 0.5846, 0.5861, 0.5876, 0.5889, 0.5904, 0.5922, 0.5934, 0.595, 0.5965, 0.598, 0.5993, 0.6011, 0.6023, 0.6038, 0.6051, 0.6066, 0.6081, 0.6093, 0.6109, 0.6121, 0.6136, 0.6152, 0.6164, 0.6179, 0.6194, 0.6207, 0.6219, 0.6234, 0.6246, 0.6259, 0.6274, 0.6286, 0.6298, 0.6314, 0.6326, 0.6341, 0.6352, 0.6366, 0.6378, 0.639, 0.6402, 0.6415, 0.643, 0.6442, 0.6454, 0.6467, 0.6479, 0.6494, 0.6503, 0.6516, 0.6528, 0.654, 0.6552, 0.6565, 0.6577, 0.6589, 0.6601, 0.6612, 0.6623, 0.6635, 0.6647, 0.6659, 0.6672, 0.6681, 0.6693, 0.6705, 0.6716, 0.6727, 0.6739, 0.6751, 0.676, 0.6773, 0.6782, 0.6794, 0.6806, 0.6815, 0.6825, 0.6837, 0.6849, 0.6858, 0.687, 0.688, 0.6892, 0.6901, 0.691, 0.6921, 0.6932, 0.6941, 0.6953, 0.6962, 0.6971, 0.6981, 0.6993, 0.7002, 0.7011, 0.7022, 0.7033, 0.7042, 0.7051, 0.7061, 0.7072, 0.7082, 0.7091, 0.71, 0.7109, 0.7118, 0.7127, 0.7137, 0.7146, 0.7155, 0.7164, 0.7173, 0.7182, 0.7192, 0.7201, 0.721, 0.7219, 0.7228, 0.7238, 0.7247, 0.7256, 0.7265, 0.7273, 0.7283, 0.729, 0.7299, 0.7308, 0.7317, 0.7324, 0.7332, 0.7342, 0.7349, 0.7357, 0.7366, 0.7375, 0.7381, 0.739, 0.74, 0.7406, 0.7415, 0.7424, 0.7432, 0.7439, 0.7447, 0.7455, 0.7464, 0.7471, 0.7479, 0.7487, 0.7495, 0.7502, 0.751, 0.7516, 0.7525, 0.7532, 0.754, 0.7547, 0.7556, 0.7562, 0.7571, 0.7577, 0.7586, 0.7592, 0.76, 0.7608, 0.7614, 0.7622, 0.7629, 0.7636, 0.7644, 0.7649, 0.7657, 0.7663, 0.7671, 0.7678, 0.7685, 0.7691, 0.7698, 0.7706, 0.7712, 0.7718, 0.7726, 0.7733, 0.7739, 0.7746, 0.7753, 0.7761, 0.7767, 0.7773, 0.7779, 0.7785, 0.7791, 0.7799, 0.7806, 0.7812, 0.7818, 0.7825, 0.7831, 0.7837, 0.7843, 0.785, 0.7857, 0.7862, 0.7868, 0.7874, 0.7881, 0.7887, 0.7893, 0.7899, 0.7906, 0.7911, 0.7917, 0.7923, 0.7929, 0.7935, 0.7941, 0.7947, 0.7953, 0.7958, 0.7965, 0.7971, 0.7977, 0.7981, 0.7987, 0.7993, 0.7999, 0.8005, 0.8012, 0.8016, 0.8022, 0.8028, 0.8033, 0.8039, 0.8044, 0.805, 0.8056, 0.8061, 0.8065, 0.8071, 0.8077, 0.8083, 0.8087, 0.8093, 0.8099, 0.8105, 0.8109, 0.8114, 0.812, 0.8124, 0.813, 0.8136, 0.814, 0.8146, 0.815, 0.8156, 0.8161, 0.8167, 0.8171, 0.8177, 0.8181, 0.8187, 0.8191, 0.8197, 0.8201, 0.8207, 0.8211, 0.8216, 0.8222, 0.8226, 0.823, 0.8236, 0.824, 0.8246, 0.825, 0.8256, 0.826, 0.8264, 0.8269, 0.8273, 0.8279, 0.8283, 0.8288, 0.8293, 0.8297, 0.8302, 0.8306, 0.8311, 0.8315, 0.832, 0.8326, 0.833, 0.8334, 0.8338, 0.8342, 0.8346, 0.8352, 0.8356, 0.836, 0.8366, 0.837, 0.8373, 0.8377, 0.8383, 0.8387, 0.8391, 0.8395, 0.84, 0.8404, 0.8407, 0.8413, 0.8417, 0.8421, 0.8424, 0.843, 0.8434, 0.8438, 0.8442, 0.8446, 0.845, 0.8454, 0.8458, 0.8462, 0.8466, 0.847, 0.8475, 0.8478, 0.8483, 0.8487, 0.8491, 0.8495, 0.8499, 0.8502, 0.8507, 0.851, 0.8515, 0.8519, 0.8523, 0.8527, 0.853, 0.8534, 0.8538, 0.8542, 0.8546, 0.855, 0.8553, 0.8556, 0.856, 0.8564, 0.8568, 0.8572, 0.8576, 0.8579, 0.8583, 0.8587, 0.8591, 0.8594, 0.8597, 0.8601, 0.8605, 0.8608, 0.8612, 0.8616, 0.862, 0.8623, 0.8626, 0.863, 0.8634, 0.8638, 0.864, 0.8645, 0.8648, 0.8651, 0.8654, 0.8658, 0.8662, 0.8664, 0.8669, 0.8672, 0.8676, 0.8679, 0.8682, 0.8686, 0.8689, 0.8692, 0.8697, 0.87, 0.8703, 0.8706, 0.8709, 0.8713, 0.8716, 0.8719, 0.8722, 0.8726, 0.8729, 0.8732, 0.8736, 0.8738, 0.8742, 0.8746, 0.8749, 0.8752, 0.8755, 0.8758, 0.8761, 0.8764, 0.8768, 0.8772, 0.8774, 0.8778, 0.878, 0.8783, 0.8786, 0.8789, 0.8792, 0.8795, 0.8798, 0.8802, 0.8806, 0.8808, 0.8811, 0.8814, 0.8818, 0.882, 0.8823, 0.8826, 0.8829, 0.8832, 0.8835, 0.8838, 0.8841, 0.8844, 0.8847, 0.885, 0.8853, 0.8856, 0.8859, 0.8862, 0.8864, 0.8867, 0.887, 0.8874, 0.8876, 0.8878, 0.8882, 0.8884, 0.8887, 0.889, 0.8893, 0.8896, 0.8899, 0.8902, 0.8905, 0.8907, 0.891, 0.8912, 0.8916, 0.8918, 0.8921, 0.8923, 0.8926, 0.8929, 0.8932, 0.8935, 0.8938, 0.894, 0.8943, 0.8945, 0.8948, 0.8951, 0.8953, 0.8956, 0.8958, 0.8962, 0.8964, 0.8966, 0.8969, 0.8972, 0.8974, 0.8977, 0.898, 0.8982, 0.8985, 0.8987, 0.899, 0.8993, 0.8995, 0.8997, 0.9, 0.9003, 0.9005, 0.9008, 0.901, 0.9013, 0.9015, 0.9018, 0.902, 0.9022, 0.9025, 0.9027, 0.903, 0.9032, 0.9034, 0.9038, 0.904, 0.9042, 0.9045, 0.9047, 0.905, 0.9052, 0.9054, 0.9056, 0.9059, 0.9062, 0.9064, 0.9066, 0.9068, 0.9071, 0.9073, 0.9076, 0.9078, 0.908};

/// Labels used for the Alfano element parameters.
const std::string
Alfano::PARAMETER_TEXT[AlfanoParamCount - NadirPointingParamCount] =
{
    "MaxPorpoisingSlew",
    "PorpoisingSlewRate",
    "MaxPorpoisingSlewRate",
    "PorpoisingSlewAxis",
    "AlfanoCostateRef",
};

/// Types of the parameters used by Alfano
const Gmat::ParameterType
Alfano::PARAMETER_TYPE[AlfanoParamCount - NadirPointingParamCount] =
{
    Gmat::REAL_TYPE,        // "MaxPorpoisingSlew"
    Gmat::REAL_TYPE,        // "PorpoisingSlewRate"
    Gmat::REAL_TYPE,        // "MaxPorpoisingSlewRate"
    Gmat::RVECTOR_TYPE,     // "PorpoisingSlewAxis"
    Gmat::REAL_TYPE,        // "AlfanoCostateRef"
};

//------------------------------------------------------------------------------
// Constructor
Alfano::Alfano(const std::string &itsName ) :
    NadirPointing(itsName),
    maxAlfanoSlew(0.0),
    porpoisingSlewRate(0.0),
    maxAlfanoSlewRate(0.0),
    alfanoCostateRef(0.0),
    alfanoSMAinit(6838.0),
    isSlewing(false),
    started(false)
{

    objectTypeNames.push_back("Alfano");
    attitudeModelName         = "Alfano";

    for (Integer i = NadirPointingParamCount; i < AlfanoParamCount; ++i)
        parameterWriteOrder.push_back(i);

    // Initialize axis of rotation
    porpoisingSlewAxis.Set(0.0, 0.0, 0.0);
}

// copy constructor
Alfano::Alfano(const Alfano& att) :
	NadirPointing(att),
    maxAlfanoSlew(att.maxAlfanoSlew),
    porpoisingSlewRate(att.porpoisingSlewRate),
    maxAlfanoSlewRate(att.maxAlfanoSlewRate),
    alfanoCostateRef(att.alfanoCostateRef),
    isSlewing(att.isSlewing),
    alfanoSMAinit(att.alfanoSMAinit),
    porpoisingSlewAxis(att.porpoisingSlewAxis),
    started(att.started)
{ 
fprintf(stdout,"Copied Alfano\n");
    for (Integer i = NadirPointingParamCount; i < AlfanoParamCount; ++i)
        parameterWriteOrder.push_back(i);
}

//------------------------------------------------------------------------------
//  GmatBase* Clone() const
//------------------------------------------------------------------------------
/**
 * This method returns a clone of the Alfano.
 *
 * @return clone of the Alfano.
 *
 */
//------------------------------------------------------------------------------
GmatBase* Alfano::Clone() const
{
	return (new Alfano(*this));
}

// Alfano Destructor
GMAT_API Alfano::~Alfano() {
}


/*********************************************************************
Helper functions for Alfano calculations
*********************************************************************/
/* Define block */
#ifndef TRUE
#define TRUE  1
#endif
#ifndef FALSE
#define FALSE  0
#endif

// Computes the Complete Elliptical Integral of the First Kind
double Alfano::CompleteEllipticIntegralFirstKind(double inval) {
    double sum=1.0, term=1.0, above=1.0, below=2.0;
    double last_sum = 0.0;

    for(int i=1; i<100; i++) {
        term *= (above/below);
        sum += pow(inval, i) * term * term;
        above += 2;
        below += 2;
        if(sum == last_sum)  {
            //printf("For k=%f, converged on iteration=%d\n", inval, i);
            break;  // Converged
        }
        last_sum = sum;
    }

    sum *= M_PI/2.0;

    return(sum);
}

// Computes the Complete Elliptical Integral of the Second Kind
double Alfano::CompleteEllipticIntegralSecondKind(double inval) {
    double sum=1.0, term=1.0, above=1.0, below=2.0;
    double last_sum = 0.0;

    for(int i=1; i<100; i++) {
        term *= (above/below);
        sum -= pow(inval, i) * term * term / above;
        above += 2;
        below += 2;
        if(sum == last_sum)  {
            //printf("For k=%f, converged on iteration=%d\n", inval, i);
            break;  // Converged
        }
        last_sum = sum;
    }

    sum *= M_PI/2.0;

    return(sum);
}

double Alfano::DerivativeCompleteEllipticIntegralFirstKind(double u, double k, double e) {
    double sq_compl_u = 1.0 - (u * u);
    return((e - sq_compl_u*k)/(u * sq_compl_u));
}

double Alfano::DerivativeCompleteEllipticIntegralSecondKind(double u, double k, double e) {
    return((e-k)/u);
}

double Alfano::alfano_P(double u, double k) {
    return(k * sqrt(1.0 - u));
}

double Alfano::alfano_Pprime(double u, double k, double dk) {
    double a = sqrt(1.0 - u);
    return(a * dk - (0.5/a)*k);
}

double Alfano::alfano_R(double u, double k, double e) {
    double su = sqrt(u);
    return(e/su + k*(su - 1.0/su));
}

double Alfano::alfano_Rprime(double u, double k, double e, double dk, double de) {
    double su = sqrt(u);
    double u_to_3halves = 1.0/pow(su, 1.5);
    double retval = k*(1.0/su + u_to_3halves)/2 + (su - 1.0/su)*dk -
                e*u_to_3halves/2 + de/su;
    return(-retval);
}

double Alfano::alfano_phi(double R, double P, double dR, double dP) {
    return((dR/dP)*P - R);
}

double Alfano::lambda(double u) {
    double k = CompleteEllipticIntegralFirstKind(u);
    double e = CompleteEllipticIntegralSecondKind(u);
    double dk = DerivativeCompleteEllipticIntegralFirstKind(u,k,e);
    double de = DerivativeCompleteEllipticIntegralSecondKind(u,k,e);
    double p = alfano_P(u,k);
    double dp = alfano_Pprime(u,k,dk);
    double r = alfano_R(u, k, e);
    double dr = alfano_Rprime(u, r, e, dk, de);
    double phi = alfano_phi(r, p, dr, dp);
    double lambda = M_PI / (2.0 * phi);
    return(lambda);
}

/*
Need to find the u value that gives lambda/sqrt(orbit_ratio)
where lambda is computed from the first input u.
*/
bool Alfano::get_control_onrev(double retval[3], double lambda1, double AOL, double SMA, double SMA_init, int more) {
    double orbit_ratio, theta;
    retval[0] = 1.0;
    retval[1] = 0.0;
    retval[2] = 0.0;

    if(SMA_init > 0.01) {
        orbit_ratio = SMA/SMA_init;
        double val;

        double lambda2 = lambda1 * sqrt(orbit_ratio);
//printf("Looking for u that gives lambda:%f\n", lambda2);
        if(lambda2 > -0.100 || lambda2 < -1.5706) return(FALSE);

        // Iteratively find control value
        double low = 0.0, high = 1.0;
        for(int i=0; i<30; i++) {
            val = (high + low) / 2.0;
            double lambda_test = lambda(val);
//printf("u:%f gives lambda:%f\n", val, lambda_test);
            if(lambda_test < lambda2) {
                high = val;
            } else {
                low = val;
            }
        }

        /*
        val = uvalues[int(round(1 + orbit_ratio/0.01) + 0.1) - 100];
        double indx1 = (1 + orbit_ratio/0.01) - 100;
        int indx2 = int((1 + orbit_ratio/0.01)) - 100;
        int indx3 = int((1 + orbit_ratio/0.01)) - 100 + 1;
        val = uvalues[indx2] + (uvalues[indx3] - uvalues[indx2]) * (indx1 - indx2) / (indx3 - indx2);
        */
        double sf = sqrt(val/(1.0-val));
        theta = atan(cos(AOL*M_PI/180.0) * sf) * more;
//printf("AOL:%f orbit_r:%f to lambda:%f gives cv(u):%f sf:%f and theta:%f\n", AOL, orbit_ratio, lambda1, val, sf, theta); fflush(stdout);
        retval[0] = cos(theta);
        retval[1] = sin(theta);
        return(TRUE);
    } else {
        return(FALSE);
    }
}
        

// Compute attitude using Real Time
void Alfano::ComputeCosineMatrixAndAngularVelocity(Real atTime) {
    Real tX;

    // NadirPointing::ComputeCosineMatrixAndAngularVelocity(atTime);
#ifdef DEBUG_NADIR_POINTING_COMPUTE
    MessageInterface::ShowMessage("In Alfano, computing at time %le\n",
            atTime);
    MessageInterface::ShowMessage("attitudeConstraintType = %s\n", attitudeConstraintType.c_str());
    if (!owningSC) MessageInterface::ShowMessage("--- owningSC is NULL!!!!\n");
    if (!refBody) MessageInterface::ShowMessage("--- refBody is NULL!!!!\n");
#endif

    if (!isInitialized || needsReinit)
    {
        Initialize();
        if (!isInitialized) {
            std::string attEx = "Alfano attitude unable to initialize";
            throw AttitudeException(attEx);
        }
    }

    // Need to do check for unset reference body here since the needsReinit
    // flag may not have been set
    if (!refBody)
    {
        std::string attEx  = "Reference body ";
        attEx             += refBodyName + " not defined for attitude of type \"";
        attEx             += "Alfano\" on spacecraft \"";
        attEx             += owningSC->GetName() + "\".";
        throw AttitudeException(attEx);
    }

    /// using a spacecraft object, *owningSC
    A1Mjd theTime(atTime);

    Rvector6 scState = ((SpaceObject*) owningSC)->GetMJ2000State(theTime);
    Rvector6 refState = refBody->GetMJ2000State(theTime);

    Rvector3 pos(scState[0] - refState[0], scState[1] - refState[1],  scState[2] - refState[2]);
    Rvector3 vel(scState[3] - refState[3], scState[4] - refState[4],  scState[5] - refState[5]);

#ifdef DEBUG_NADIR_POINTING_COMPUTE
    MessageInterface::ShowMessage("   scState  = %s\n", scState.ToString().c_str());
    MessageInterface::ShowMessage("   refState = %s\n", refState.ToString().c_str());
    MessageInterface::ShowMessage("   pos      = %s\n", pos.ToString().c_str());
    MessageInterface::ShowMessage("   vel      = %s\n", vel.ToString().c_str());
#endif

    // Error message
    std::string attErrorMsg = "Nadir Pointing attitude model is singular and/or ";
    attErrorMsg            += "undefined for Spacecraft \"";
    attErrorMsg            += owningSC->GetName() + "\".";
    if ((bodyAlignmentVector.GetMagnitude() < 1.0e-5 )  ||
            (bodyConstraintVector.GetMagnitude() < 1.0e-5)  ||
            (bodyAlignmentVector.GetUnitVector()*
             bodyConstraintVector.GetUnitVector()
             > ( 1.0 - 1.0e-5))                        ||
            (pos.GetMagnitude() < 1.0e-5)                   ||
            (vel.GetMagnitude() < 1.0e-5)                   ||
            (Cross(pos,vel).GetMagnitude() < 1.0e-5)        )
    {
        throw AttitudeException(attErrorMsg);
    }

    Rvector3 normal = Cross(pos,vel);

    Rvector3 xhat = pos/pos.GetMagnitude();
    Rvector3 yhat = normal/normal.GetMagnitude();
    Rvector3 zhat = Cross(xhat,yhat);

    Rvector3 referenceVector;
    Rvector3 constraintVector;


#ifdef DEBUG_NADIR_POINTING_COMPUTE
    MessageInterface::ShowMessage("   refBody = <%s>\n", refBody->GetName().c_str());
    MessageInterface::ShowMessage("   normal  = %s\n", normal.ToString().c_str());
    MessageInterface::ShowMessage("   xhat    = %s\n", xhat.ToString().c_str());
    MessageInterface::ShowMessage("   yhat    = %s\n", yhat.ToString().c_str());
    MessageInterface::ShowMessage("   zhat    = %s\n", zhat.ToString().c_str());
#endif

    // RiI calculation (from inertial to LVLH)
    Rmatrix33 RIi;
    RIi(0,0) = xhat(0);
    RIi(1,0) = xhat(1);
    RIi(2,0) = xhat(2);
    RIi(0,1) = yhat(0);
    RIi(1,1) = yhat(1);
    RIi(2,1) = yhat(2);
    RIi(0,2) = zhat(0);
    RIi(1,2) = zhat(1);
    RIi(2,2) = zhat(2);


    // Set alignment/constraint vector in body frame
    constraintVector[0] = 0;
    constraintVector[1] = 1;
    constraintVector[2] = 0;
    if ( attitudeConstraintType == "OrbitNormal" && isSlewing)
    {
        referenceVector[0] = -1;
        referenceVector[1] = 0;
        referenceVector[2] = 0;

        Real AOL = owningSC->GetRealParameter("TA") + owningSC->GetRealParameter("AOP");
        Real SMA = owningSC->GetRealParameter("SMA");
        double retval[3];
        get_control_onrev(retval, alfanoCostateRef, AOL, SMA, alfanoSMAinit, -1);

        constraintVector[0] = 0;
        constraintVector[1] = retval[0]; //cos(0.0 / 180.0 * M_PI);
        constraintVector[2] = retval[1]; //sin(0.0 / 180.0 * M_PI);
    }
    else if ( attitudeConstraintType == "Velocity" )
    {
        referenceVector[0] = -1;
        referenceVector[1] = 0;
        referenceVector[2] = 0;

        constraintVector[0] = 0;
        constraintVector[1] = 0;
        constraintVector[2] = -1;
    }

    // RBi calculation using TRIAD (from LVLH to body frame)
    //Rmatrix33 RiB = TRIAD( bodyAlignmentVector, bodyConstraintVector, referenceVector, constraintVector );
    Rmatrix33 RiB = TRIAD( bodyAlignmentVector, bodyConstraintVector, porpoisingSlewAxis, constraintVector );

    // the rotation matrix (from body to inertial)
    dcm = RIi*RiB;
    // the final rotation matrix (from inertial to body)
    dcm =  dcm.Transpose();

    // We do NOT calculate angular velocity for Nadir.
    // TODO : Throw AttitudeException?? write warning - but what to return?
    callCount++;
}

// Initializes the Alfano maneuver and validates settings
bool GMAT_API Alfano::Initialize(void) {
	bool  retValue = NadirPointing::Initialize();

    // Make sure axis is specified reasonably and normalize it
    Real length = 0.0;
    for(int i=0; i<porpoisingSlewAxis.GetSize(); i++) {
        length += porpoisingSlewAxis[i] * porpoisingSlewAxis[i];
    }
    if(length < 1.0e-10) {
        isInitialized = false;
        return(isInitialized);
    }
    length = sqrt(length);
    for(int i=0; i<porpoisingSlewAxis.GetSize(); i++) {
        porpoisingSlewAxis[i] /= length;
    }

    // Make sure specified rate is supported by rotaters

	return(retValue);
}

//------------------------------------------------------------------------------
//  Integer  GetParameterID(const std::string &str) const
//------------------------------------------------------------------------------
/**
 * This method returns the parameter ID, given the input parameter string.
 *
 * @param <str> string for the requested parameter.
 *
 * @return ID for the requested parameter.
 *
 */
//------------------------------------------------------------------------------
Integer Alfano::GetParameterID(const std::string &str) const
{
   for (Integer i = getBaseParameterCount(); i < getParameterCount(); i++)
   {
       if (str == PARAMETER_TEXT[i - getBaseParameterCount()]) {
           fprintf(stdout, "Parameter %s is id:%d\n", str.c_str(), i);
           return i;
       }
   }

   return NadirPointing::GetParameterID(str);
}

// And their names and types
Gmat::ParameterType Alfano::GetParameterType(const Integer id) const {
    if (id >= getBaseParameterCount() && id < getParameterCount())
        return PARAMETER_TYPE[id - getBaseParameterCount()];

    return(NadirPointing::GetParameterType(id));
}

// Sets the specified real parameter to the specified value
Real Alfano::SetRealParameter(const Integer id, const Real value) {
    switch (id) {
        case MAXPORPOISINGSLEW:
            if(value >= 0.0) {
                maxAlfanoSlew = value;
            } else {
                AttitudeException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "MaxAlfanoSlew", "0.0 <= Real Number");
                throw he;
            }
            return(maxAlfanoSlew);
        case PORPOISINGSLEWRATE:
            if(value >= 0.0) {
                porpoisingSlewRate = value;
            } else {
                AttitudeException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "AlfanoSlewRate", "0.0 <= Real Number");
                throw he;
            }
            return(porpoisingSlewRate);
        case ALFANOCOSTATEREF:
            if(value > 0.0 && value < 1.0) {
                alfanoCostateRef = lambda(value);
            } else if(value > -M_PI/2.0 && value < 0.0) {
                alfanoCostateRef = value;
            } else {
                AttitudeException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "AlfanoCostateRef", "0.0 < Real Number < 1.0 or -1.5707 < Real Number < 0.0");
                throw he;
            }
            return(alfanoCostateRef);
    }

    return NadirPointing::SetRealParameter(id, value);
}



// Sets the specified bool parameter to the specified value
bool Alfano::SetBooleanParameter(const Integer id, const bool value) {
	return(NadirPointing::SetBooleanParameter(id, value));
}

   // Sets the specified Boolean parameter to the specified value
bool GMAT_API Alfano::SetBooleanParameter(const std::string &label, const bool value) {
	Integer id = GetParameterID(label);
    return SetBooleanParameter(id, value);
}

   // Sets the specified real parameter to the specified value
Real GMAT_API Alfano::SetRealParameter(const Integer id, const Real value, const Integer index) {
    if(id == PORPOISINGSLEWAXIS) {
        if(index < 0 || index >= porpoisingSlewAxis.GetSize()) {
            throw AttitudeException("Index out of bounds setting the porpoising axis on " +
                instanceName);
        }

        porpoisingSlewAxis[index] = value;
        return(porpoisingSlewAxis[index]);
    }

    return(NadirPointing::SetRealParameter(id, value, index));
}

       // And their names and types
std::string GMAT_API Alfano::GetParameterText(const Integer id) const {
    if (id >= getBaseParameterCount() && id < getParameterCount())
        return PARAMETER_TEXT[id - getBaseParameterCount()];

    return(NadirPointing::GetParameterText(id));
}

// Kicks off the porpoising maneuver at the current orientation
bool GMAT_API Alfano::StartPorpoising(void) {
    bool retValue = true;

    Spacecraft *sc = (Spacecraft*) owningSC;
    if(!sc) {
        throw AttitudeException("No Spacecraft set for Alfano Attitude:" +
                instanceName);
    }
    double atTime = sc->GetEpoch();
    fprintf(stdout,"Start time:%s\n", sc->GetEpochString().c_str());

    alfanoSMAinit = sc->GetRealParameter("SMA");
    fprintf(stdout,"Initial SMA:%f\n", alfanoSMAinit);
    fflush(stdout);

/*
    // Compute u array for various orbit ratios from 1.0 to 10.0
    for(int indx=0; indx<901; indx++) {
        double orbit_ratio = 1.0 + (10.0 - 1.0) * indx / 901;
        double low = 0.0, high = 1.0;
        double lambda2 = alfanoCostateRef * sqrt(orbit_ratio);
        double val;
        for(int i=0; i<40; i++) {
            val = (high + low) / 2.0;
            double lambda_test = lambda(val);
//printf("u:%f gives lambda:%f\n", val, lambda_test);
            if(lambda_test < lambda2) {
                high = val;
            } else {
                low = val;
            }
        }
        printf("Lambda:%f  old:%.4f  u:%.4f\n", alfanoCostateRef, uvalues[indx], val);
        uvalues[indx] = val;
    }
*/

    // Start the maneuver
    isSlewing = true;
    isStopping = false;

    callCount = 0;
    return(retValue);
}

// Terminates the current porpoising maneuver at the current orientation
bool GMAT_API Alfano::StopPorpoising(void) {
    bool  retValue = true;

    // Stop the maneuver
    isSlewing = false;
    isStopping = false;
    started = false;

    printf("Call Count:%d\n", callCount);
    fflush(stdout);
    return(retValue);
}

// Terminates the current porpoising maneuver at the zero orientation
bool GMAT_API Alfano::SlowPorpoising(void) {
    Spacecraft *sc = (Spacecraft*) owningSC;
    fprintf(stdout,"Stop time:%s\n", sc->GetEpochString().c_str());
    printf("Call Count:%d\n", callCount);
    fflush(stdout);
    
    if(isSlewing) {
        // Note that we need to go to 0
        isStopping = true;
        started = false;
        return(true);
    } else{
        // If we are not currently porpoising, it is wrong to go back to 0
        return(false);
    }
}

