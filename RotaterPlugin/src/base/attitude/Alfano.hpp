
/**
*********************************************************
  Class Alfano
  Created by john
  Created Thu Mar 17 04:20:12 PDT 2022
*********************************************************
  Modification History
  Date        Programmer       Change

*********************************************************
*********************************************************
*/

#ifndef _Alfano_H_
#define _Alfano_H_

// includes block
#include "NadirPointing.hpp"
#include "AlfanoDefs.hpp"

// declarations block

/// Implements an attitude module to do porpoising
/**
    The Alfano class implements an attitude module to do porpoising.
It is based on the NadirPointing class in that it points one axis to the
center of its reference body and rotates around that axis.
*/

class GMAT_API Alfano : public NadirPointing
{     

    private:

    protected:

        // Helper functions for the Alfano optimization
    	double CompleteEllipticIntegralFirstKind(double inval);
    	double CompleteEllipticIntegralSecondKind(double inval);
    	double DerivativeCompleteEllipticIntegralFirstKind(double u, double k, double e);
    	double DerivativeCompleteEllipticIntegralSecondKind(double u, double k, double e);
    	double alfano_P(double u, double k);
    	double alfano_Pprime(double u, double k, double dk);
    	double alfano_R(double u, double k, double e);
    	double alfano_Rprime(double u, double k, double e, double dk, double de);
    	double alfano_phi(double R, double P, double dR, double dP);
    	double lambda(double u);
        bool get_control_onrev(double retval[3], double u, double AOL, double SMA, double SMA_init, int more);

        virtual void ComputeCosineMatrixAndAngularVelocity(Real atTime);

        /// Published parameters of Alfanos
        enum {
            MAXPORPOISINGSLEW = NadirPointingParamCount,
            PORPOISINGSLEWRATE,
            MAXPORPOISINGSLEWRATE,
            PORPOISINGSLEWAXIS,
            ALFANOCOSTATEREF,
            AlfanoParamCount
        };

        /// And their names and types
        static const std::string
            PARAMETER_TEXT[AlfanoParamCount - NadirPointingParamCount];
        static const Gmat::ParameterType
            PARAMETER_TYPE[AlfanoParamCount - NadirPointingParamCount];

        // Parameter access methods - overridden from Attitude
        virtual Integer GetParameterID(const std::string &str) const;
        virtual Gmat::ParameterType GetParameterType(const Integer id) const;
        virtual std::string GetParameterText(const Integer id) const;

        /// Virtual method to return number of parameters to check
        virtual Integer getParameterCount() const {
            return(AlfanoParamCount);
        }

        /// Virtual method to get base class parameter count
        virtual Integer getBaseParameterCount() const {
            return(NadirPointingParamCount);
        }

        /// Maximum slew angle in degrees
        Real    maxAlfanoSlew;

        /// Desired maximum slew rate in degrees per second
        Real    porpoisingSlewRate;

        /// Maximum slew rate allowed in degrees per second
        Real    maxAlfanoSlewRate;

        /// Axis around which to porpoise
        Rvector3 porpoisingSlewAxis;

        /// Alfano costate reference value from which lambda is derived
        // This value should be between 0.0 and 1.0 exclusive
        Real    alfanoCostateRef;

        /// Alfano initial SMA - needed for algorithm and set at start
        Real    alfanoSMAinit;

        /// State
        bool    isSlewing;
        bool    isStopping;

        /// Has propagator called us even once
        bool    started;

        /// Array of u values for various orbit ratios
        static Real    uvalues[902];

        int callCount;

    public:

	/// Kicks off the porpoising maneuver at the current orientation
	/**
	The StartPorpoising method kicks off the porpoising maneuver
	@return Returns true if successful and false otherwise
	*/
	bool StartPorpoising(void);

	/// Terminates the current porpoising maneuver at the current orientation
	/**
	The StopPorpoising method terminates the current porpoising maneuver at 
    the current orientation.
	@return Returns true if successful and false otherwise
	*/
	bool StopPorpoising(void);

	/// Terminates the current porpoising maneuver at the zero orientation
	/**
	The SlowPorpoising method terminates the current porpoising maneuver at 
    the current orientation and rotates back to zero slew and ends.
	@return Returns true if successful and false otherwise
	*/
	bool SlowPorpoising(void);

    /// # Insert single line doxygen comment here
    /**
    The Rotater::Initialize method # Insert multiline doxygen comment here
    @return Returns # value description
    */
    bool Initialize(void);

    /// Sets the specified real parameter to the specified value
    /**
    The SetRealParameter method # Insert multiline doxygen comment here
    @return Returns # value description
    @param id const Integer # Argument description
    @param value const Real # Argument description
    @param index const Integer # Argument description
    */
    virtual Real SetRealParameter(const Integer id, const Real value);
    virtual Real SetRealParameter(const Integer id, const Real value, const Integer index);

    /// Sets the specified Boolean parameter to the specified value
    /**
    The SetBooleanParameter method sets the specified Boolean parameter to the specified value
    @return Returns the boolean value that was set
    @param id const Integer Index of value in parameter array
    @param label const string Name of value in parameter array
    @param value const Boolean Value to set parameter to
    */
    virtual bool SetBooleanParameter(const Integer id, const bool value);
    virtual bool SetBooleanParameter(const std::string &label, const bool value);

    /// Clones this instance of a Alfano
    /**
    The Clone method  Insert multiline doxygen comment herones this instance of a Alfano
    @return Returns newly cloned copy of this object
    */
    virtual GmatBase* Clone(void) const;


	/// Alfano Constructor
	/**
	Constructor for the Alfano class
	*/
    Alfano(const std::string &itsName = "");

    /// Alfano copy constructor
    Alfano(const Alfano& att);

	/// Alfano Destructor
	/**
	Destructor for the Alfano class
	*/
	virtual	~Alfano();

	/// Alfano Self test method
	/**
	Self test method for the Alfano class
	Silently returns TRUE if successful and returns FALSE and 
	outputs descriptive text if unsuccessful.
	*/
	static int self_test(void);

};

#endif

