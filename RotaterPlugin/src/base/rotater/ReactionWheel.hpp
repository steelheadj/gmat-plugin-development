
/**
********************************************************
  Class ReactionWheel
  Created by john
  Created Tue Mar 8 14:30:24 PST 2022
*********************************************************
  Modification History
  Date        Programmer       Change

*********************************************************
*********************************************************
*/

#ifndef _ReactionWheel_H_
#define _ReactionWheel_H_

// includes block
#include "Rotater.hpp"

// declarations block

/// # Enter Doxygen single line comment here
/**
    # Enter Doxygen multi-line comment here
*/

class ROTATER_API ReactionWheel : public Rotater {

    private:

    protected:

    public:

	/// # Clones this instance of a ReactionWheel
	/**
	The Clone method # Insert multiline doxygen comment here
	@return Returns # value description
	*/
	virtual GmatBase* Clone(void) const;

	/// ReactionWheel Constructor
	/**
	Constructor for the ReactionWheel class
	*/
	ReactionWheel(const std::string &nomme);

	/// ReactionWheel Constructor
	/**
	Constructor for the ReactionWheel class
	*/
	ReactionWheel(const ReactionWheel& source);

	/// ReactionWheel Destructor
	/**
	Destructor for the ReactionWheel class
	*/
	virtual	~ReactionWheel() {};

	/// ReactionWheel Self test method
	/**
	Self test method for the ReactionWheel class
	Silently returns TRUE if successful and returns FALSE and 
	outputs descriptive text if unsuccessful.
	*/
	static int self_test(void);

};

#endif

