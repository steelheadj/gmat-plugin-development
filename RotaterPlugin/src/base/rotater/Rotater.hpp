/**
********************************************************
  Class Rotater
  Created by john
  Created Tue Mar 8 14:21:05 PST 2022
*********************************************************
  Modification History
  Date        Programmer       Change

*********************************************************
*********************************************************
*/

#ifndef _Rotater_H_
#define _Rotater_H_

// includes block
#include "Hardware.hpp"
#include "RotaterDefs.hpp"

// declarations block
class Spacecraft;

/// Implements a base class for Hardware that rotates a spacecraft
/**
    The Rotater class implements a base class for Hardware that
rotates a spacecraft.  These include reaction wheels, vernier
rockets or thrusters, geomagnetic induction coils, etc.
*/

class ROTATER_API Rotater : public Hardware {

    private:

    protected:

        /// Published parameters of Rotaters
        enum {
            MAXPOSRATE = HardwareParamCount,
            MAXNEGRATE,
            MAXPOSACCEL,
            MAXNEGACCEL,
//            AXIS,
            RotaterParamCount
        };

        /// And their names and types
        static const std::string
            PARAMETER_TEXT[RotaterParamCount - HardwareParamCount];
        static const Gmat::ParameterType
            PARAMETER_TYPE[RotaterParamCount - HardwareParamCount];

        // Parameter access methods - overridden from Hardware
        virtual Integer GetParameterID(const std::string &str) const;
        virtual Gmat::ParameterType GetParameterType(const Integer id) const;
        //virtual std::string  GetParameterText(const Integer id) const;
        //virtual std::string  GetParameterTypeString(const Integer id) const;

        /// Virtual method to return number of parameters to check
        virtual Integer getParameterCount() const;
        /// Virtual method to get base class parameter count
        virtual Integer getBaseParameterCount() const;

        /// Maximum spin rate in the positive direction around axis
        Real    maxPosRate;
        /// Maximum spin rate in the negative direction around axis
        Real    maxNegRate;
        /// Maximum spin acceleration in the positive direction
        Real    maxPosAccel;
        /// Maximum spin acceleration in the negative direction
        Real    maxNegAccel;

    public:

	/// # Insert single line doxygen comment here
	/**
	The Rotater::Initialize method # Insert multiline doxygen comment here
	@return Returns # value description
	*/
	bool Initialize(void);

	/// Sets the specified real parameter to the specified value
	/**
	The SetRealParameter method # Insert multiline doxygen comment here
	@return Returns # value description
	@param id const Integer # Argument description
	@param value const Real # Argument description
	*/
	virtual Real SetRealParameter(const Integer id, const Real value);

	/// Rotater Constructor
	/**
	Constructor for the Rotater class
	*/
	Rotater(const std::string &typeStr, const std::string &nomme);

	/// Rotater Constructor
	/**
	Constructor for the Rotater class
	*/
	Rotater(const Rotater& source);

	/// Rotater Destructor
	/**
	Destructor for the Rotater class
	*/
	virtual	~Rotater() {};

	/// Rotater Self test method
	/**
	Self test method for the Rotater class
	Silently returns TRUE if successful and returns FALSE and 
	outputs descriptive text if unsuccessful.
	*/
	static int self_test(void);

};

#endif
