#include "Rotater.hpp"
#include "ObjectReferencedAxes.hpp"
#include "Spacecraft.hpp"
#include "StringUtil.hpp"
#include "HardwareException.hpp"
#include "MessageInterface.hpp"
#include <sstream>
#include <math.h>          // for pow(real, real)

/// Labels used for the Rotater element parameters.
const std::string
Rotater::PARAMETER_TEXT[RotaterParamCount - HardwareParamCount] =
{
    "MaxPositiveRotationRate",
    "MaxNegativeRotationRate",
    "MaxPositiveRotationAcceleration",
    "MaxNegativeRotationAcceleration",
    //"Axis",
};

/// Types of the parameters used by Rotaters.
const Gmat::ParameterType
Rotater::PARAMETER_TYPE[RotaterParamCount - HardwareParamCount] =
{
    Gmat::REAL_TYPE,        // "MaxPosRate"
    Gmat::REAL_TYPE,        // "MaxNegRate"
    Gmat::REAL_TYPE,        // "MaxPosAccel"
    Gmat::REAL_TYPE,        // "MaxNegAccel"
    //Gmat::ENUMERATION_TYPE, // "Axis"
};

// Rotater Constructor
Rotater::Rotater(const std::string &typeStr, const std::string &nomme) :
    Hardware (Gmat::HARDWARE, typeStr, nomme),
    maxPosRate(0.0),
    maxNegRate(0.0),
    maxPosAccel(0.0),
    maxNegAccel(0.0)
{
    typeName = "Rotater";
}

// Rotater Constructor
Rotater::Rotater(const Rotater& source) :
    Hardware(source),
    maxPosRate(source.maxPosRate),
    maxNegRate(source.maxNegRate),
    maxPosAccel(source.maxPosAccel),
    maxNegAccel(source.maxNegAccel)
{
}


// Virtual method to return number of parameters to check
Integer Rotater::getParameterCount() const {
	return(RotaterParamCount);
}

// Virtual method to get base class parameter count
Integer Rotater::getBaseParameterCount() const {
	return(HardwareParamCount);
}

// Sets the specified real parameter to the specified value
Real Rotater::SetRealParameter(const Integer id, const Real value) {
    switch (id) {
        case MAXPOSRATE:
            if(value >= 0.0) {
                maxPosRate = value;
            } else {
                HardwareException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "MaxPositiveRotationRate", "0.0 <= Real Number");
                throw he;
            }
            return(maxPosRate);
        case MAXNEGRATE:
            if(value >= 0.0) {
                maxNegRate = value;
            } else {
                HardwareException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "MaxNegativeRotationRate", "0.0 <= Real Number");
                throw he;
            }
            return(maxNegRate);
        case MAXPOSACCEL:
            if(value >= 0.0) {
                maxPosAccel = value;
            } else {
                HardwareException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "MaxPositiveRotationAcceleration", "0.0 <= Real Number");
                throw he;
            }
            return(maxPosAccel);
        case MAXNEGACCEL:
            if(value >= 0.0) {
                maxNegAccel = value;
            } else {
                HardwareException he;
                he.SetDetails(errorMessageFormat.c_str(),
                    GmatStringUtil::ToString(value, GetDataPrecision()).c_str(),
                    "MaxNegativeRotationAcceleration", "0.0 <= Real Number");
                throw he;
            }
            return(maxNegAccel);
    }

	return Hardware::SetRealParameter(id, value);
}

//------------------------------------------------------------------------------
//  Integer  GetParameterID(const std::string &str) const
//------------------------------------------------------------------------------
/**
 * This method returns the parameter ID, given the input parameter string.
 *
 * @param <str> string for the requested parameter.
 *
 * @return ID for the requested parameter.
 *
 */
//------------------------------------------------------------------------------
Integer Rotater::GetParameterID(const std::string &str) const
{
   for (Integer i = getBaseParameterCount(); i < getParameterCount(); i++)
   {
      if (str == PARAMETER_TEXT[i - getBaseParameterCount()])
         return i;
   }
  
   return Hardware::GetParameterID(str);
}

// And their names and types
Gmat::ParameterType Rotater::GetParameterType(const Integer id) const {
    if (id >= getBaseParameterCount() && id < getParameterCount())
        return PARAMETER_TYPE[id - getBaseParameterCount()];

	return(Hardware::GetParameterType(id));
}

// # Insert single line doxygen comment here
bool Rotater::Initialize(void) {
	bool  retValue = true;

	return(retValue);
}

