#include "ReactionWheel.hpp"
#include "ObjectReferencedAxes.hpp"
#include "Spacecraft.hpp"
#include "StringUtil.hpp"
#include "HardwareException.hpp"
#include "MessageInterface.hpp"
#include <sstream>
#include <math.h>          // for pow(real, real)

// ReactionWheel Constructor
ReactionWheel::ReactionWheel(const std::string &nomme) :
    Rotater("ReactionWheel", nomme)
{
    isInitialized = true;
    typeName = "ReactionWheel";
}

// ReactionWheel Constructor
ReactionWheel::ReactionWheel(const ReactionWheel& source) :
    Rotater(source)
{
    isInitialized = true;
}

// # Clones this instance of a ReactionWheel
GmatBase* ReactionWheel::Clone(void) const {
	return(new ReactionWheel(*this));
}

